import Vuex from 'vuex'
import Vue from 'vue'

Vue.use(Vuex)

export default new Vuex.Store(({
    state:{
        user:{
            name:"BoBoBoy",
            address:"Malaysia"
        }
    },

    getters:{
        getUser: state =>{
            return  state.user.name
        },

        getAddress: state=>{
            return state.user.address
        }

    },

    mutations:{
        updateName(state, name){
            state.user.name = name
        },
        updateAddress(state, address){
            state.user.address = address
        }
    },
    actions:{
        updateUser(context, object_data){
            context.commit('updateName', object_data.name)
            context.commit('updateAddress',object_data.address)
        }
    }
}))